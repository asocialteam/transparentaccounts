package cz.csas.homework.appprovider.annotation

@Target(AnnotationTarget.CLASS)
@Retention(AnnotationRetention.RUNTIME)
annotation class Menu(val value: Int = -1, val toolbarId: Int = -1, val drawerId: Int = -1)
