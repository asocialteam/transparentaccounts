package cz.csas.homework.appprovider.view.controller

import android.app.Activity
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout

class DrawerController {
    enum class Side {
        LEFT, RIGHT
    }

    private var drawer: DrawerLayout

    constructor(ac: Activity, drawerId: Int) {
        drawer = ac.findViewById(drawerId)
    }

    fun getDrawer(): DrawerLayout {
        return drawer
    }

    fun isMenuOpen(side: Side): Boolean {
        return if (side == Side.LEFT)
            drawer.isDrawerOpen(GravityCompat.START)
        else
            drawer.isDrawerOpen(GravityCompat.END)
    }

    fun isMenuOpen(): Boolean {
        return drawer.isDrawerOpen(GravityCompat.START) || drawer.isDrawerOpen(GravityCompat.END)
    }

    fun close() {
        drawer.closeDrawers()
    }

    fun open(side: Side) {
        drawer.openDrawer(if (side == Side.LEFT)
            GravityCompat.START
        else
            GravityCompat.END)
    }

    fun disable(side: Side) {
        drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED,
                if (side == Side.LEFT) GravityCompat.START else GravityCompat.END)
    }

    fun disable() {
        drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED)
    }

    fun enable() {
        drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED)
    }

    fun enable(side: Side) {
        drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED,
                if (side == Side.LEFT) GravityCompat.START else GravityCompat.END)
    }
}