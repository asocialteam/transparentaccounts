package cz.csas.homework.appprovider.view.controller

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.animation.ValueAnimator
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.widget.Toolbar
import android.view.View
import android.view.animation.DecelerateInterpolator
import cz.csas.homework.appprovider.R
import cz.csas.homework.appprovider.model.pattern.ActivityPattern

class ToolbarController(val activity: ActivityPattern, toolbarId: Int) {

    enum class IconStates {
        HAMBURGER, ARROW, CROSS
    }

    private val toolbar: Toolbar = activity.findViewById(toolbarId)
    private var toggle: ActionBarDrawerToggle? = null
    private val drawerController: DrawerController?

    init {
        activity.setSupportActionBar(toolbar)
        activity.supportActionBar?.elevation  = 0.0f
        drawerController = activity.getDrawerController()
        initMaterialIcon()
    }

    fun setTitle(title: String) {
        toolbar.title = title
        toolbar.invalidate()
    }

    fun getToggle(): ActionBarDrawerToggle? {
        return toggle
    }

    private fun initMaterialIcon() {
        if (drawerController != null) {
            toggle = ActionBarDrawerToggle(activity, drawerController.getDrawer(),
                    toolbar, R.string.app_name, R.string.app_name)
            setBasicNavigationOnClick()
        }
    }

    private fun setBasicNavigationOnClick() {
        drawerController?.getDrawer()?.addDrawerListener(toggle as DrawerLayout.DrawerListener)
        toggle?.syncState()
        activity.getDrawerController()?.enable()
        setNavigationOnClick( View.OnClickListener{
            if(drawerController != null) {
                if (drawerController.isMenuOpen(DrawerController.Side.LEFT)) {
                    drawerController.close()
                } else {
                    drawerController.open(DrawerController.Side.LEFT)
                }
            }
        })
    }

    fun setNavigationOnClick(onClick: View.OnClickListener?) {
        toolbar.setNavigationOnClickListener(onClick)
    }

    private fun getBackStackNavigationOnClick(): Animator.AnimatorListener {
        return object : AnimatorListenerAdapter() {
            override fun onAnimationStart(animation: Animator) {
                drawerController?.disable()
                setNavigationOnClick(null)
            }

            override fun onAnimationEnd(animation: Animator) {
                setNavigationOnClick(View.OnClickListener { activity.onBackPressed() })
            }
        }
    }

    private fun startMaterialIconAnimation(from: Float, to: Float, mListener: Animator.AnimatorListener?) {
        if (drawerController != null) {
            val anim = ValueAnimator.ofFloat(from, to)
            anim.addUpdateListener { valueAnimator ->
                val slideOffset = valueAnimator.animatedValue as Float
                toggle?.onDrawerSlide(drawerController.getDrawer(), slideOffset)
            }
            if (mListener != null) {
                anim.addListener(mListener)
            }
            anim.interpolator = DecelerateInterpolator()
            anim.duration = activity.resources.getInteger(R.integer.ANIMATION_DURATION).toLong()
            anim.start()
        }
    }

    fun setMaterialIcon(iconStates: IconStates) {
        when (iconStates) {
            ToolbarController.IconStates.HAMBURGER -> {
                initMaterialIcon()
                startMaterialIconAnimation(1f, 0f, null)
            }
            ToolbarController.IconStates.ARROW -> startMaterialIconAnimation(0f, 1f, getBackStackNavigationOnClick())
            ToolbarController.IconStates.CROSS -> toolbar.setNavigationIcon(R.drawable.ic_close)
        }
    }


}