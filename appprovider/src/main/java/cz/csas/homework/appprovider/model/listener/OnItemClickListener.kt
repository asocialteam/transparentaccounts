package cz.csas.homework.appprovider.model.listener

interface OnItemClickListener<T> {

    fun onItemClick(arg: T)

}