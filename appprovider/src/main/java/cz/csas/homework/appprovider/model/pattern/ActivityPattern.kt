package cz.csas.homework.appprovider.model.pattern

import android.os.Bundle
import android.support.annotation.IdRes
import android.support.annotation.LayoutRes
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import butterknife.ButterKnife
import cz.csas.homework.appprovider.manager.FragmentNavigationManager
import cz.csas.homework.appprovider.model.listener.ActivityListener
import cz.csas.homework.appprovider.view.controller.DrawerController
import cz.csas.homework.appprovider.view.controller.ToolbarController
import cz.csas.homework.dataprovider.manager.ApiManager

abstract class ActivityPattern : AppCompatActivity, ActivityListener {

    private val apiManager: ApiManager = ApiManager()

    private var fragmentNavigationManager: FragmentNavigationManager? = null
    private var toolbarController: ToolbarController? = null
    private var drawerController: DrawerController? = null
    private var menu: Menu? = null

    @LayoutRes private val activityLayoutId: Int
    @IdRes private val fragmentContainerId: Int

    private val menuIdsDef: cz.csas.homework.appprovider.annotation.Menu?

    init {
        val menuClass = cz.csas.homework.appprovider.annotation.Menu::class.java
        menuIdsDef = if (javaClass.isAnnotationPresent(menuClass))
            javaClass.getAnnotation(menuClass)
        else null
    }

    constructor(@LayoutRes activityLayoutId: Int) : this(activityLayoutId, -1)

    constructor(@LayoutRes activityLayoutId: Int, @IdRes fragmentContainerId: Int) {
        this.activityLayoutId = activityLayoutId
        this.fragmentContainerId = fragmentContainerId
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(activityLayoutId)

        if(menuIdsDef != null){
            if(menuIdsDef.drawerId != -1){
                drawerController = DrawerController(this, menuIdsDef.drawerId)
            }

            if(menuIdsDef.toolbarId != -1){
                toolbarController = ToolbarController(this, menuIdsDef.toolbarId)
            }
        }

        if(fragmentContainerId != -1) {
            fragmentNavigationManager = FragmentNavigationManager(this, fragmentContainerId)
        }
        ButterKnife.bind(this, this)
        onCreated()
    }

    abstract fun onCreated()

    override fun getToolbarController() = toolbarController

    override fun getDrawerController() = drawerController

    override fun getFragmentNavigationManager() = fragmentNavigationManager

    override fun getApiManager() = apiManager

    override fun getMenu() = menu

    override fun onBackPressed() {

        val backStackCount = supportFragmentManager.backStackEntryCount

        if (!this.onBackPressed(backStackCount)) {
            return
        }

        if (backStackCount == 0) {
            super.onBackPressed()
        } else {
            if (backStackCount == 1) {
                toolbarController?.setMaterialIcon(ToolbarController.IconStates.HAMBURGER)
            }

            supportFragmentManager.popBackStack()
        }
    }

    abstract fun onBackPressed(backStackCount: Int): Boolean

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val menuId = menuIdsDef?.value ?: return false

        return if(menuId != -1) {
            menuInflater.inflate(menuId, menu)
            this.menu = menu
            true
        } else false
    }

}