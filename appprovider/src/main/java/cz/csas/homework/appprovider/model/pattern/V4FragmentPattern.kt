package cz.csas.homework.appprovider.model.pattern

import android.content.Context
import android.os.Bundle
import android.support.annotation.LayoutRes
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import butterknife.ButterKnife
import cz.csas.homework.appprovider.model.listener.ActivityListener

abstract class V4FragmentPattern(@LayoutRes private val layoutId: Int) : Fragment() {

    private lateinit var rootView: View
    private lateinit var activityListener: ActivityListener

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        try {
            activityListener = context as ActivityListener
        } catch (e: ClassCastException) {
            throw ClassCastException(context.toString() + " has to implement ActivityPattern.Getters")
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        rootView = inflater.inflate(layoutId, container, false)
        ButterKnife.bind(this, rootView)
        onFragmentCreated()
        return rootView
    }

    abstract fun onFragmentCreated()

    abstract fun getName(): String

    protected fun getToolbarController() = activityListener.getToolbarController()

    protected fun getDrawerController() = activityListener.getDrawerController()

    protected fun getFragmentNavigationManager() = activityListener.getFragmentNavigationManager()

    protected fun getApiManager() = activityListener.getApiManager()

    protected fun getMenu() = activityListener.getMenu()

    protected fun getRootView() = rootView

    override fun onResume() {
        super.onResume()
    }

    override fun onPause() {
        super.onPause()
    }

}