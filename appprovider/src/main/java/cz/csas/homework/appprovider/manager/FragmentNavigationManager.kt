package cz.csas.homework.appprovider.manager

import android.os.Bundle
import cz.csas.homework.appprovider.model.pattern.ActivityPattern
import cz.csas.homework.appprovider.model.pattern.V4FragmentPattern
import cz.csas.homework.appprovider.view.controller.ToolbarController

class FragmentNavigationManager(private val activity: ActivityPattern, private val fragmentContainerId: Int) {

    private lateinit var currentFragment: V4FragmentPattern

    fun replaceFragment(frag: V4FragmentPattern, data: Bundle, addToBackStack: Boolean) {
        try {
            if (activity.fragmentManager.findFragmentByTag(frag.getName()) == null) {
                frag.arguments = data
                val ft = activity.supportFragmentManager.beginTransaction()
                ft.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out, android.R.anim.fade_in, android.R.anim.fade_out)

                if (addToBackStack) {
                    //set event for back button and disable leftSideMenu
                    if (activity.supportFragmentManager.backStackEntryCount == 0 && activity.getToolbarController() != null) {
                        activity.getToolbarController()?.setMaterialIcon(ToolbarController.IconStates.ARROW)
                    }

                    ft.add(fragmentContainerId, frag, frag.getName())
                    ft.addToBackStack(frag.getName())
                } else {
                    popBackStackToHome()
                    currentFragment = frag
                    ft.replace(fragmentContainerId, frag)
                }

                ft.commit()
            }
        } catch (ex: Exception) {
        }

    }

    fun setFragment(frag: V4FragmentPattern, bundle: Bundle? = null) {
        frag.arguments = bundle
        popBackStackToHome()
        currentFragment = frag
        activity.supportFragmentManager
                .beginTransaction()
                .add(fragmentContainerId, currentFragment, currentFragment.getName())
                .commit()
    }

    fun popBackStackToHome() {
        if (activity.fragmentManager.backStackEntryCount > 0) {
            activity.getToolbarController()?.setMaterialIcon(ToolbarController.IconStates.HAMBURGER)
        }
        activity.fragmentManager.popBackStack(null, android.support.v4.app.FragmentManager.POP_BACK_STACK_INCLUSIVE)
    }

}
