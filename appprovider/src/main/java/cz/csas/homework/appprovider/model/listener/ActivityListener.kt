package cz.csas.homework.appprovider.model.listener

import android.view.Menu
import cz.csas.homework.appprovider.manager.FragmentNavigationManager
import cz.csas.homework.appprovider.view.controller.DrawerController
import cz.csas.homework.appprovider.view.controller.ToolbarController
import cz.csas.homework.dataprovider.manager.ApiManager

interface ActivityListener {
    fun getToolbarController(): ToolbarController?
    fun getDrawerController(): DrawerController?
    fun getFragmentNavigationManager(): FragmentNavigationManager?
    fun getApiManager(): ApiManager
    fun getMenu(): Menu?
}