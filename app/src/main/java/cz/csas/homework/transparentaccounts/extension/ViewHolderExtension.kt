package cz.csas.homework.transparentaccounts.extension

import android.view.View
import cz.csas.homework.appprovider.model.listener.OnItemClickListener
import cz.csas.homework.dataprovider.extension.toDate
import cz.csas.homework.dataprovider.extension.toFormatedString
import cz.csas.homework.dataprovider.model.entity.Account
import cz.csas.homework.dataprovider.model.entity.Transaction
import cz.csas.homework.transparentaccounts.R
import cz.csas.homework.transparentaccounts.view.adapter.AccountsRecyclerAdapter
import cz.csas.homework.transparentaccounts.view.adapter.TransactionsRecyclerAdapter
import java.math.BigDecimal

fun AccountsRecyclerAdapter.ViewHolder.bind(item: Account, listener: OnItemClickListener<Account>) {
    textViewAccountNumber.text = StringBuilder(item.accountNumber).append("/").append(item.bankCode)
    textViewCurrency.text = item.currency
    textViewBalance.text = StringBuilder().append(item.balance)
    textViewName.text = item.name

    if (item.description.isNullOrEmpty()) {
        textViewDescription.visibility = View.GONE
    } else {
        textViewDescription.text = item.description
    }
    textViewNote.text = item.note
    cardView.setOnClickListener {
        listener.onItemClick(item)
    }
}

fun TransactionsRecyclerAdapter.ViewHolder.bind(item: Transaction, listener: OnItemClickListener<Transaction>) {

    val value = item.amount?.value

    when {
        value?.compareTo(BigDecimal.ZERO)!! > 0 -> viewAmountType.setBackgroundResource(R.drawable.account_detail_item_value_positive)
        value < BigDecimal.ZERO -> viewAmountType.setBackgroundResource(R.drawable.account_detail_item_value_negative)
        else -> viewAmountType.setBackgroundResource(R.drawable.account_detail_item_value_neutral)
    }

    textViewValue.text = StringBuilder().append(value)
    textViewCurrency.text = item.amount?.currency
    textViewDesc.text = item.typeDescription
    textViewSender.text = item.sender?.name
    textViewDate.text = item.processingDate?.toDate()?.toFormatedString()


}