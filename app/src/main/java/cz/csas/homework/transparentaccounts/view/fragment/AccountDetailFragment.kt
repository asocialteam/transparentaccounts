package cz.csas.homework.transparentaccounts.view.fragment

import android.app.AlertDialog
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.TextView
import butterknife.BindView
import cz.csas.homework.appprovider.model.listener.OnItemClickListener
import cz.csas.homework.appprovider.model.pattern.V4FragmentPattern
import cz.csas.homework.dataprovider.extension.toDate
import cz.csas.homework.dataprovider.extension.toFormatedString
import cz.csas.homework.dataprovider.model.entity.Account
import cz.csas.homework.dataprovider.model.entity.Transaction
import cz.csas.homework.dataprovider.model.listener.ApiListener
import cz.csas.homework.dataprovider.model.response.TransactionsResponse
import cz.csas.homework.transparentaccounts.R
import cz.csas.homework.transparentaccounts.view.adapter.TransactionsRecyclerAdapter

class AccountDetailFragment : V4FragmentPattern(R.layout.fragment_account_detail), OnItemClickListener<Transaction> {
    companion object {
        const val BUNDLE_KEY_ACCOUNT = "account"
    }

    @BindView(R.id.fragment_account_detail_text_name)
    lateinit var textViewName: TextView
    
    @BindView(R.id.fragment_account_detail_text_account_number)
    lateinit var textViewAccountNumber: TextView
    
    @BindView(R.id.fragment_account_detail_text_currency)
    lateinit var textViewCurrency: TextView
    
    @BindView(R.id.fragment_account_detail_text_balance)
    lateinit var textViewBalance: TextView
    
    @BindView(R.id.fragment_account_detail_text_description)
    lateinit var textViewDescription: TextView

    @BindView(R.id.fragment_account_detail_text_transparency)
    lateinit var textViewTransparency: TextView
    
    @BindView(R.id.fragment_account_detail_text_note)
    lateinit var textViewNote: TextView 

    @BindView(R.id.fragment_account_detail_recycler_transactions)
    lateinit var recyclerViewTransactions: RecyclerView

    @BindView(R.id.fragment_account_detail_swipe)
    lateinit var swipeRefreshLayout: SwipeRefreshLayout

    private var actualPage: Int = 0
    private var transactionsResponse: TransactionsResponse? = null
    private var displayedTransactions: ArrayList<Transaction> = arrayListOf()
    private var accountNumber: String? = null

    override fun onFragmentCreated() {
        val account: Account? = arguments?.getParcelable(BUNDLE_KEY_ACCOUNT)
        if(account != null) {
            accountNumber = account.accountNumber

            textViewName.text = account.name
            textViewAccountNumber.text = StringBuilder(account.accountNumber).append("/").append(account.bankCode)
            textViewCurrency.text = account.currency
            textViewBalance.text = StringBuilder().append(account.balance)
            if (account.description.isNullOrEmpty()) {
                textViewDescription.visibility = View.GONE
            } else {
                textViewDescription.text = account.description
            }

            textViewTransparency.text = String.format(getString(R.string.transparency_from_to), account.transparencyFromEncoded?.toDate()?.toFormatedString(), account.transparencyToEncoded?.toDate()?.toFormatedString())

            textViewNote.text = account.note
            getApiManager().loadAccountTransactions(accountNumber!!, actualPage, loadAccountTransactionsCallback())
            swipeRefreshLayout.isRefreshing = true
        }
        recyclerViewTransactions.layoutManager = LinearLayoutManager(context)
        recyclerViewTransactions.addOnScrollListener(onBottomReachedListener())

        swipeRefreshLayout.setOnRefreshListener {
            displayedTransactions.clear()
            getApiManager().loadAccountTransactions(accountNumber!!, 0, loadAccountTransactionsCallback())
        }
    }

    override fun getName(): String = javaClass.simpleName

    private fun displayTransactions(){
        recyclerViewTransactions.adapter = TransactionsRecyclerAdapter(displayedTransactions, this)
    }

    override fun onItemClick(arg: Transaction) {

    }

    private fun loadAccountTransactionsCallback(): ApiListener<TransactionsResponse>.() -> Unit = {
        onFailure = { response ->
            swipeRefreshLayout.isRefreshing = false
            AlertDialog.Builder(activity)
                    .setTitle(getString(R.string.request_failure))
                    .setMessage(response?.message)
                    .setPositiveButton(R.string.ok, null)
                    .create().show()
        }

        onResponse = { response ->
            if (response.isSuccessful) {
                transactionsResponse = response.body()
                displayedTransactions.addAll(transactionsResponse?.transactions!!)

                if(transactionsResponse?.pageNumber == 0) {
                    displayTransactions()
                }else {
                    recyclerViewTransactions.layoutManager.requestLayout()
                }

                swipeRefreshLayout.isRefreshing = false
            }else {
                AlertDialog.Builder(activity)
                        .setTitle(response.code())
                        .setMessage(response.message())
                        .setPositiveButton(R.string.ok, null)
                        .create().show()
            }
        }
    }

    private fun onBottomReachedListener() = object : RecyclerView.OnScrollListener() {
        override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
            if (swipeRefreshLayout.isRefreshing)
                return
            val layoutManager = recyclerViewTransactions.layoutManager
            val visibleItemCount = layoutManager.childCount
            val totalItemCount = layoutManager.itemCount
            val pastVisibleItems = (layoutManager as LinearLayoutManager).findFirstVisibleItemPosition()
            if (pastVisibleItems + visibleItemCount >= totalItemCount - 5) {
                if(actualPage < transactionsResponse?.pageCount!!){
                    getApiManager().loadAccountTransactions(accountNumber!!, ++actualPage, loadAccountTransactionsCallback())
                }
            }
        }
    }
}
