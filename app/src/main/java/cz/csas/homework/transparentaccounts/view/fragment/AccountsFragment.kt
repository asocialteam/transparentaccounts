package cz.csas.homework.transparentaccounts.view.fragment

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.os.Bundle
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.MotionEvent
import android.view.View
import android.widget.AdapterView
import butterknife.BindView
import cz.csas.homework.appprovider.model.listener.OnItemClickListener
import cz.csas.homework.appprovider.model.pattern.V4FragmentPattern
import cz.csas.homework.dataprovider.model.entity.Account
import cz.csas.homework.dataprovider.model.listener.ApiListener
import cz.csas.homework.dataprovider.model.response.AccountsResponse
import cz.csas.homework.transparentaccounts.R
import cz.csas.homework.transparentaccounts.view.adapter.AccountsRecyclerAdapter

class AccountsFragment: V4FragmentPattern(R.layout.fragment_accounts),OnItemClickListener<Account>{
    @BindView(R.id.fragment_accounts_recycler)
    lateinit var recyclerViewAccounts: RecyclerView

    @BindView(R.id.fragment_accounts_swipe)
    lateinit var swipeRefreshLayout: SwipeRefreshLayout

    private var actualPage: Int = 0
    private var accountsResponse: AccountsResponse? = null
    private var displayedAccounts: ArrayList<Account> = arrayListOf()

    override fun onFragmentCreated() {
        getApiManager().loadAccounts(actualPage, loadAccountsCallback())
        swipeRefreshLayout.isRefreshing = true

        recyclerViewAccounts.layoutManager = LinearLayoutManager(context)
        recyclerViewAccounts.addOnScrollListener(onBottomReachedListener())

        swipeRefreshLayout.setOnRefreshListener {
            displayedAccounts.clear()
            getApiManager().loadAccounts(0, loadAccountsCallback())
        }
    }

    override fun getName(): String = javaClass.simpleName

    private fun displayAccounts(){
        recyclerViewAccounts.adapter = AccountsRecyclerAdapter(displayedAccounts,this)
    }

    override fun onItemClick(arg: Account) {
            val bundle = Bundle()

            bundle.putParcelable(AccountDetailFragment.BUNDLE_KEY_ACCOUNT, arg)

            getFragmentNavigationManager()?.replaceFragment(AccountDetailFragment(), bundle, true)
    }

    private fun loadAccountsCallback(): ApiListener<AccountsResponse>.() -> Unit = {
        onFailure = { response ->
            swipeRefreshLayout.isRefreshing = false
            AlertDialog.Builder(activity)
                    .setTitle(getString(R.string.request_failure))
                    .setMessage(response?.message)
                    .setPositiveButton(R.string.ok, null)
                    .create().show()
        }

        onResponse = { response ->
            if (response.isSuccessful) {
                accountsResponse = response.body()
                displayedAccounts.addAll(accountsResponse?.accounts!!)

                if(accountsResponse?.pageNumber == 0) {
                    displayAccounts()
                }else {
                    recyclerViewAccounts.layoutManager.requestLayout()
                }

                swipeRefreshLayout.isRefreshing = false
            }else{
                AlertDialog.Builder(activity)
                        .setTitle(response.code())
                        .setMessage(response.message())
                        .setPositiveButton(R.string.ok, null)
                        .create().show()
            }
        }
    }

    private fun onBottomReachedListener() = object : RecyclerView.OnScrollListener() {
        override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
            if (swipeRefreshLayout.isRefreshing)
                return
            val layoutManager = recyclerViewAccounts.layoutManager
            val visibleItemCount = layoutManager.childCount
            val totalItemCount = layoutManager.itemCount
            val pastVisibleItems = (layoutManager as LinearLayoutManager).findFirstVisibleItemPosition()
            if (pastVisibleItems + visibleItemCount >= totalItemCount - 5) {
                if(actualPage < accountsResponse?.pageCount!!){
                    getApiManager().loadAccounts(++actualPage, loadAccountsCallback())
                }
            }
        }
    }
}
