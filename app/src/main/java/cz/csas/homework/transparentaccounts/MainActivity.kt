package cz.csas.homework.transparentaccounts

import cz.csas.homework.appprovider.annotation.Menu
import cz.csas.homework.appprovider.model.pattern.ActivityPattern
import cz.csas.homework.transparentaccounts.view.fragment.AccountsFragment


@Menu(R.menu.menu_activity_main, R.id.activity_main_toolbar, R.id.activity_main_drawer_layout)
class MainActivity : ActivityPattern(R.layout.activity_main, R.id.activity_main_fragment_container) {


    override fun onCreated() {
        getFragmentNavigationManager()?.setFragment(AccountsFragment())
    }

    override fun onBackPressed(backStackCount: Int): Boolean {
        return true
    }

}
