package cz.csas.homework.transparentaccounts.view.adapter

import android.support.constraint.ConstraintLayout
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import cz.csas.homework.appprovider.model.listener.OnItemClickListener
import cz.csas.homework.dataprovider.model.entity.Transaction
import cz.csas.homework.transparentaccounts.R
import cz.csas.homework.transparentaccounts.extension.bind
import cz.csas.homework.transparentaccounts.extension.inflate
import kotlinx.android.synthetic.main.fragment_account_detail_item.view.*

class TransactionsRecyclerAdapter(private val listOfTransactions: ArrayList<Transaction>, private val onItemClickListener: OnItemClickListener<Transaction>) : RecyclerView.Adapter<TransactionsRecyclerAdapter.ViewHolder>() {


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        internal val constraintLayout: ConstraintLayout = itemView.fragment_account_detail_item_constraint
        internal val textViewDesc: TextView = itemView.fragment_account_detail_item_text_desc
        internal val viewAmountType: View = itemView.fragment_account_detail_item_value_type
        internal val textViewValue: TextView = itemView.fragment_account_detail_item_text_balance
        internal val textViewCurrency: TextView = itemView.fragment_account_detail_item_text_currency
        internal val textViewSender: TextView = itemView.fragment_account_detail_item_text_sender
        internal val textViewDate: TextView = itemView.fragment_account_detail_item_text_date
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(parent.inflate(R.layout.fragment_account_detail_item))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(listOfTransactions[position], onItemClickListener)

    override fun getItemCount() = listOfTransactions.size
}
