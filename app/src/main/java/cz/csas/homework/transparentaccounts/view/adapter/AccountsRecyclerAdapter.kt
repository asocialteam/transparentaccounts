package cz.csas.homework.transparentaccounts.view.adapter

import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import cz.csas.homework.appprovider.model.listener.OnItemClickListener
import cz.csas.homework.dataprovider.model.entity.Account
import cz.csas.homework.transparentaccounts.R
import cz.csas.homework.transparentaccounts.extension.inflate
import cz.csas.homework.transparentaccounts.extension.bind
import kotlinx.android.synthetic.main.fragment_accounts_item.view.*

class AccountsRecyclerAdapter(private val listOfAccounts: ArrayList<Account>, private val onItemClickListener: OnItemClickListener<Account>) : RecyclerView.Adapter<AccountsRecyclerAdapter.ViewHolder>() {

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        internal val cardView: CardView = itemView.fragment_accounts_item_card
        internal val textViewAccountNumber: TextView = itemView.fragment_accounts_item_text_account_number
        internal val textViewCurrency: TextView = itemView.fragment_accounts_item_text_currency
        internal val textViewBalance: TextView = itemView.fragment_accounts_item_text_balance
        internal val textViewName: TextView = itemView.fragment_accounts_item_text_name
        internal val textViewDescription: TextView = itemView.fragment_accounts_item_text_description
        internal val textViewNote: TextView = itemView.fragment_accounts_item_text_note
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(parent.inflate(R.layout.fragment_accounts_item))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(listOfAccounts[position], onItemClickListener)

    override fun getItemCount() = listOfAccounts.size

}

