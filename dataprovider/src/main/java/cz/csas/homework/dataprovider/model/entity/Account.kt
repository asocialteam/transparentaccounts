package cz.csas.homework.dataprovider.model.entity

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import cz.csas.homework.dataprovider.extension.toDate
import java.math.BigDecimal
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.time.temporal.TemporalAccessor
import java.time.temporal.TemporalAdjusters
import java.util.*

class Account() : Parcelable{

    @SerializedName("accountNumber") var accountNumber: String? = null
    @SerializedName("bankCode") var bankCode: String? = null
    @SerializedName("transparencyFrom") var transparencyFromEncoded: String? = null
    @SerializedName("transparencyTo") var transparencyToEncoded: String? = null
    @SerializedName("publicationTo") var publicationToEncoded: String? = null
    @SerializedName("actualizationDate") var actualizationDateEncoded: String? = null
    @SerializedName("balance") var balance: BigDecimal? = null
    @SerializedName("currency") var currency: String? = null
    @SerializedName("name") var name: String? = null
    @SerializedName("description") var description: String? = null
    @SerializedName("note") var note: String? = null
    @SerializedName("iban") var iban: String? = null
    @SerializedName("statements") var statements: ArrayList<String>? = null

    constructor(parcel: Parcel) : this() {
        accountNumber = parcel.readString()
        bankCode = parcel.readString()
        transparencyFromEncoded = parcel.readString()
        transparencyToEncoded = parcel.readString()
        publicationToEncoded = parcel.readString()
        actualizationDateEncoded = parcel.readString()
        currency = parcel.readString()
        name = parcel.readString()
        description = parcel.readString()
        note = parcel.readString()
        iban = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(accountNumber)
        parcel.writeString(bankCode)
        parcel.writeString(transparencyFromEncoded)
        parcel.writeString(transparencyToEncoded)
        parcel.writeString(publicationToEncoded)
        parcel.writeString(actualizationDateEncoded)
        parcel.writeString(currency)
        parcel.writeString(name)
        parcel.writeString(description)
        parcel.writeString(note)
        parcel.writeString(iban)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Account> {
        override fun createFromParcel(parcel: Parcel): Account {
            return Account(parcel)
        }

        override fun newArray(size: Int): Array<Account?> {
            return arrayOfNulls(size)
        }
    }

}