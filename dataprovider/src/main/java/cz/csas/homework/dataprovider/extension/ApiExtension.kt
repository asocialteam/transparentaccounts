package cz.csas.homework.dataprovider.extension

import android.annotation.SuppressLint
import cz.csas.homework.dataprovider.model.listener.ApiListener
import retrofit2.Call
import java.text.ParsePosition
import java.text.SimpleDateFormat
import java.util.*

fun <T> Call<T>.enqueue(listener: ApiListener<T>.() -> Unit){
    val apiListener = ApiListener<T>()
    listener.invoke(apiListener)
    this.enqueue(apiListener)
}

@SuppressLint("SimpleDateFormat")
fun String.toDate(): Date = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(this)

@SuppressLint("SimpleDateFormat")
fun Date.toFormatedString(): String = SimpleDateFormat("dd.MM.yyyy").format(this)
