package cz.csas.homework.dataprovider.manager

import com.google.gson.GsonBuilder
import cz.csas.homework.dataprovider.BuildConfig
import cz.csas.homework.dataprovider.extension.enqueue
import cz.csas.homework.dataprovider.model.listener.ApiListener
import cz.csas.homework.dataprovider.model.pattern.ApiPattern
import cz.csas.homework.dataprovider.model.response.AccountsResponse
import cz.csas.homework.dataprovider.model.response.TransactionsResponse
import retrofit2.Callback
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class ApiManager {

    private val api: ApiPattern

    init {
        val gson = GsonBuilder()
                .setLenient()
                .create()

        val retrofit = Retrofit.Builder()
                .baseUrl(BuildConfig.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build()

        api = retrofit.create(ApiPattern::class.java)
    }

    fun loadAccounts(page: Int, listener: ApiListener<AccountsResponse>.() -> Unit) = api.getAccounts(
            BuildConfig.API_KEY,
            page,
            null,
            null
    ).enqueue(listener)

    fun loadAccountTransactions(accountNumber: String, page: Int, listener: ApiListener<TransactionsResponse>.() -> Unit) = api.getAccountTransactions(
            BuildConfig.API_KEY,
            accountNumber,
            page,
            null,
            "processingDate",
            null,
            null,
            null,
            null
    ).enqueue(listener)

}