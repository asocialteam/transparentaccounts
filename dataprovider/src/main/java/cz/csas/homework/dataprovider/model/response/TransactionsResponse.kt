package cz.csas.homework.dataprovider.model.response

import com.google.gson.annotations.SerializedName
import cz.csas.homework.dataprovider.model.entity.Transaction
import cz.csas.homework.dataprovider.model.pattern.PageResponsePattern

class TransactionsResponse: PageResponsePattern() {
    @SerializedName("transactions") var transactions: ArrayList<Transaction>? = null
}