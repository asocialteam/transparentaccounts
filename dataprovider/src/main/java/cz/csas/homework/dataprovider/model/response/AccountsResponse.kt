package cz.csas.homework.dataprovider.model.response

import com.google.gson.annotations.SerializedName
import cz.csas.homework.dataprovider.model.entity.Account
import cz.csas.homework.dataprovider.model.pattern.PageResponsePattern

class AccountsResponse: PageResponsePattern() {
    @SerializedName("accounts") var accounts: ArrayList<Account>? = null
}