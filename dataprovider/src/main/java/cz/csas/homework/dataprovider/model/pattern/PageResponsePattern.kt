package cz.csas.homework.dataprovider.model.pattern

import com.google.gson.annotations.SerializedName

open class PageResponsePattern{
    @SerializedName("pageNumber") var pageNumber: Int = -1
    @SerializedName("pageCount") var pageCount: Int = -1
    @SerializedName("pageSize") var pageSize: Int = -1
    @SerializedName("recordCount") var recordCount: Int = -1
    @SerializedName("nextPage") var nextPage: Int = -1
}