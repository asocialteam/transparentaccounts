package cz.csas.homework.dataprovider.model.pattern

import android.app.Activity
import android.content.Context
import android.content.SharedPreferences

abstract class SettingPattern {

    companion object {
        const val SHARED_PREFERENCES_FILE_NAME = "TransparentAccounts"
    }

    private val sharedPreferences: SharedPreferences

    constructor(activity: Activity){
        sharedPreferences = activity.getSharedPreferences(SHARED_PREFERENCES_FILE_NAME, Context.MODE_PRIVATE)
    }

    protected fun <T> getPreference(key: String, type: Class<T>): T {
        return when (type::class.java) {
            String::class.java -> type.cast(sharedPreferences.getString(key, ""))
            Int::class.java -> type.cast(sharedPreferences.getInt(key, 0))
            Float::class.java -> type.cast(sharedPreferences.getFloat(key, 0f))
            Boolean::class.java -> type.cast(sharedPreferences.getBoolean(key, false))
            else -> throw Exception("Unsupported type: $type")
        }
    }

    protected fun <T> setPreference(key: String, value: T) {
        when(value){
            is String -> sharedPreferences.edit().putString(key, value).apply()
            is Int -> sharedPreferences.edit().putInt(key, value).apply()
            is Float -> sharedPreferences.edit().putFloat(key, value).apply()
            is Boolean -> sharedPreferences.edit().putBoolean(key, value).apply()
            else -> throw Exception("Unsupported type of value: $value")
        }
    }

}