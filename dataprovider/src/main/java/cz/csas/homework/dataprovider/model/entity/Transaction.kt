package cz.csas.homework.dataprovider.model.entity

import com.google.gson.annotations.SerializedName
import java.math.BigDecimal

class Transaction{

    @SerializedName("amount") var amount: Amount? = null
    @SerializedName("sender") var sender: Sender? = null
    @SerializedName("receiver") var receiver: Receiver? = null
    @SerializedName("type") var type: String? = null
    @SerializedName("dueDate") var dueDate: String? = null
    @SerializedName("processingDate") var processingDate: String? = null
    @SerializedName("typeDescription") var typeDescription: String? = null

    class Amount {
        @SerializedName("value") var value: BigDecimal? = null
        @SerializedName("precision") var precision: Int? = null
        @SerializedName("currency") var currency: String? = null
    }
    
    class Sender {
        @SerializedName("accountNumber") var accountNumber: String? = null
        @SerializedName("bankCode") var bankCode: String? = null
        @SerializedName("iban") var iban: String? = null
        @SerializedName("specificSymbol") var specificSymbol: String? = null
        @SerializedName("specificSymbolParty") var specificSymbolParty: String? = null
        @SerializedName("variableSymbol") var variableSymbol: String? = null
        @SerializedName("constantSymbol") var constantSymbol: String? = null
        @SerializedName("name") var name: String? = null
        @SerializedName("description") var description: String? = null
    }

    class Receiver{
        @SerializedName("accountNumber") var accountNumber: String? = null
        @SerializedName("bankCode") var bankCode: String? = null
        @SerializedName("iban") var iban: String? = null
    }
}