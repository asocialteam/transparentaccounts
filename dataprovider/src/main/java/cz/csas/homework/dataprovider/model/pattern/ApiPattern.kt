package cz.csas.homework.dataprovider.model.pattern

import cz.csas.homework.dataprovider.model.response.AccountsResponse
import cz.csas.homework.dataprovider.model.response.TransactionsResponse
import cz.csas.homework.dataprovider.model.entity.Account
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiPattern {

    @GET("transparentAccounts/health")
    fun getHealth(
            @Header("WEB-API-key") apiKey: String
    ): Call<Any>

    @GET("transparentAccounts")
    fun getAccounts(
            @Header("WEB-API-key") apiKey: String,
            @Query("page") page: Int?,
            @Query("size") size: Int?,
            @Query("filter") filter: String?
    ): Call<AccountsResponse>

    @GET("transparentAccounts/{account_number}")
    fun getAccountDetail(
            @Header("WEB-API-key") apiKey: String,
            @Path(value = "account_number", encoded = true) accountNumber: String
    ): Call<Account>

    @GET("transparentAccounts/{account_number}/transactions")
    fun getAccountTransactions(
            @Header("WEB-API-key") apiKey: String,
            @Path(value = "account_number", encoded = true) accountNumber: String,
            @Query("page") page: Int?,
            @Query("size") size: Int?,
            @Query("sort") sort: String?,
            @Query("order") order: String?,
            @Query("dateFrom") dateFrom: String?,
            @Query("dateTo") dateTo: String?,
            @Query("filter") filter: String?
    ): Call<TransactionsResponse>

}